import express from 'express'
import bodyParser from 'body-parser'

function run() {

    const port = process.argv[2] || 3000

    const app = express()

    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(express.static('public'))

    app.get('/', (req, res, next) => {res.redirect('/index.html');})
    app.post('/doit/', (req, res, next) => {
        // TODO
        res.send("OK")
    })
    
    app.listen(port, () => console.log('Web running on port ' + port))
}

run()



