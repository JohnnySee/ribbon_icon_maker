import maker from './ribbonmaker'

function run() {
    // Check argument count
    if (process.argv.length < 3) {
        console.log("Error: Wrong arguments")
        console.log("")
        console.log("Usage: node index.js INPUT_FILE [OUTPUT_DIR]")
        process.exit(1)
    }

    // Load parameters
    const INPUT_FILE = process.argv[2]
    const OUTPUT_DIR = process.argv[3] || "out/"

    const presets = [
        { filename: "Android/%VARIANT%/res/mipmap/store.png", size: 512 },
        { filename: "Android/%VARIANT%/res/mipmap/ldpi/ic_launcher.png", size: 36 },
        { filename: "Android/%VARIANT%/res/mipmap/ldpi/ic_launcher.png", size: 36 },
        { filename: "Android/%VARIANT%/res/mipmap/mdpi/ic_launcher.png", size: 48 },
        { filename: "Android/%VARIANT%/res/mipmap/hdpi/ic_launcher.png", size: 72 },
        { filename: "Android/%VARIANT%/res/mipmap/xhdpi/ic_launcher.png", size: 96 },
        { filename: "Android/%VARIANT%/res/mipmap/xxhdpi/ic_launcher.png", size: 144 },
        { filename: "Android/%VARIANT%/res/mipmap/xxxhdpi/ic_launcher.png", size: 192 },
    
        { filename: "iOS/%VARIANT%/store.png", size: 1024 },
        { filename: "iOS/%VARIANT%/app_icon@2x.png", size: 120 },
        { filename: "iOS/%VARIANT%/app_icon@3x.png", size: 180 }
    ]

    maker(INPUT_FILE, OUTPUT_DIR, presets)
}

run()