import { createCanvas, loadImage } from 'canvas'
import fs from 'fs'
import path from 'path'
import find from 'list-files'
import mkdirp from 'mkdirp'

export default function generateVariants(backgroundFile, outputDir) {
    var presets = JSON.parse(fs.readFileSync('presets.json', 'utf8'));
    find(async (results) => {
        for (var i in results) {
            const filename = results[i]
            await generateVariant(backgroundFile, filename, outputDir, presets)
        }
    }, {
        dir: 'variants',
        name: 'png'
    })
}

async function generateVariant(backgroundFile, overlayFile, outputDir, presets) {
    const variantName = path.basename(overlayFile, ".png")
    console.log("Variant: " + variantName)
    for (var i in presets) {
        const preset = presets[i]
        var filename = preset.filename.replace("%VARIANT%", variantName)
        var fullPath = outputDir + filename
        const dirname = path.dirname(fullPath)
        mkdirp.sync(dirname)
        await draw(backgroundFile, overlayFile, fullPath, preset.size)
    }
}

async function draw(backgroundFile, overlayFile, outputFile, size) {    
    const canvas = createCanvas(size, size)
    const ctx = canvas.getContext('2d')
    ctx.imageSmoothingEnabled = true
    await drawImageFromFile(ctx, backgroundFile, size)
    await drawImageFromFile(ctx, overlayFile, size)
    await writeCanvasToFile(canvas, outputFile)
    console.log("Exporting " + outputFile)
}

function drawImageFromFile(ctx, filename, size) {
    return new Promise(
        (resolve, reject) => {
            loadImage(filename).then((image) => {
                ctx.drawImage(image, 0, 0, size, size)
                resolve()
            })
        }
    )
}

function writeCanvasToFile(canvas, filename) {
    return new Promise(
        (resolve, reject) => {
            const out = fs.createWriteStream(filename)
            const stream = canvas.pngStream()
            stream.on('data', (chunk) => {
                out.write(chunk)
            })
            stream.on('end', () => {
                resolve()
            })
            out.on('finish', () => {})
        }
    )
}